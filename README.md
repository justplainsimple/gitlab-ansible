# Ansible Scripts to Setup GitLab

This project provisions a new GitLab server.

For testing, or for local use, this project also includes a [Vagrant](http://vagrantup.com) environment that can be provisioned.

